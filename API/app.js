const express = require('express');
const app = express();

const { mongoose } = require('./db/mongoose');
const bodyParser = require('body-parser');

/* Load-in Mongoose models */
const { Question } = require('./db/models/question.model');
const { User } = require('./db/models/user.model');

/* ROUTES HANDLERS */


/* LIST ROUTES */


/* LOAD MIDDLEWARE */
app.use(bodyParser.json()); // This middleware will parse the request body of the HTTP request


// CORS HEADERS MIDDLEWARE
app.use(function (req, res, next) {
    // Update to match the domain you will make the request from
    res.header("Access-Control-Allow-Origin", "*");
    // We have a header to allow all the methods on HTTP
    res.header("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS, PUT, PATCH, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});



// We use app constant to access al HTTP methods from express framework
app.listen(3000, () => {
    console.log("Server is listening on prot 3000");
})


/* Questions CRUD HTTP functionality */

/**
 * GET /questions
 * Purpose: Get all questions published and unpublished
 */
app.get('/questions', (req, res) => {
    // We want to return an array of the questions in the database
    Question.find({}).then((questions) => {
        res.send(questions);
    });
});

/**
 * GET /questions
 * Purpose: Get all questions published and unpublished
 */
app.get('/questions/:id', (req, res) => {
    // We want to return an array of the questions in the database
    Question.findById({ _id:req.params.id
    }).then((questions) => {
        res.send(questions);
    });
});


/**
 * POST /questions
 * Purpose: Create question
 */
app.post('/questions', (req, res) => {
    // We want to create a new question and return the new question document back to the user (which includes ID)
    // The question information field will be past in via the JSON request body
    // To create a new question we firstly have to read the data from the JSON request body
    let newQuestion = new Question({
        title: req.body.title,
        option1: req.body.option1,
        option2: req.body.option2,
        option3: req.body.option3,
        option4: req.body.option4,
        correctAnswer: req.body.correctAnswer
    });

    // We save and return the full question document via console(ID included)
    newQuestion.save().then((questionDoc) => {
        res.send(questionDoc);
    });
});


/**
 * PATCH /question/:id
 * Purpose: Update a specific question
 */
app.patch('/questions/:id', (req, res) => {
    // We want to update the specific question (question document with ID in the URL) with the new values specified in JSON body format
    Question.findByIdAndUpdate(
        { _id: req.params.id } // We find the id, compares with db one and update with $set
        ,
        { $set: req.body }).then(() => {
            res.send({ message: 'Question updated succesfully' });
        }).catch((e) => {
            console.log(e);
        });
    // $set, MongoDB keyword, it will update the question that finds with the condition with the content of the request
});


/**
 * DELETE /question/:id
 * Purpose: Delete an specific question
 */
app.delete('/questions/:id', (req, res) => {
    // We want to delete the specific question (question document with ID in the URL)
    Question.findByIdAndRemove(
        { _id: req.params.id }).then((questionRemovedDoc) => {
            res.send(questionRemovedDoc);
        });
});



/* USER ROUTES */

/**
 * GET /users
 * Purpose: Get all questions published and unpublished
 */
app.get('/users', (req, res) => {
    // We want to return an array of the questions in the database
    User.find({}).then((users) => {
        res.send(users);
    });
});

/**
 * POST /users
 * Purpose: Sign up
 */
app.post('/users', (req, res) => {
    // User sign up
    let body = req.body;
    let newUser = new User(body);

    // We save and return the full question document via console(ID included)
    newUser.save().then((userDoc) => {
        res.send(userDoc);
    }).catch((e) => {
        res.status(400).send(e);
    })
})


/**
 * DELETE /users/:id
 * Purpose: Delete an specific user
 */
app.delete('/users/:id', (req, res) => {
    // We want to delete the specific user (question document with ID in the URL)
    User.findByIdAndRemove(
        { _id: req.params.id }).then((userRemovedDoc) => {
            res.send(userRemovedDoc);
        });
});
