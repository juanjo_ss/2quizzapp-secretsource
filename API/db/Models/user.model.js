/**
 * Module dependencies.
 */
const mongoose = require('mongoose');
const crypto = require('crypto');

/**
 * A Validation function for local strategy properties
 */
const validateLocalStrategyProperty = function (property) {
    return ((this.provider !== 'local' && !this.updated) || property.length);
};

/**
 * A Validation function for local strategy password
 */
const validateLocalStrategyPassword = function (password) {
    return (this.provider !== 'local' || validator.isLength(password, 6));
};



const userSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: 'Username already exists',
        required: 'Please fill in a username',
        trim: true
    },
    password: {
        type: String,
        required: true,
        minlength: 1,
        validate: [validateLocalStrategyPassword, 'Password should be longer']
    },
    roles: {
        type: [{
            type: String,
            enum: ['player', 'admin']
        }],
        default: ['player']
    },
    firstname: {
        type: String,
        trim: true
    },
    lastname: {
        type: String,
        trim: true
    },
    phone: {
        type: String,
        trim: true
    }
});


// We create the Model and export it
const User = mongoose.model('User', userSchema);

module.exports = { User };