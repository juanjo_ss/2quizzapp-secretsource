const mongoose = require('mongoose');

const QuestionSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        minlength: 1,
        trim:true // Removes whitespace characters, including null, or the specified characters from the beginning and end of a string.
    },
    option1: {
        type: String,
        required: true,
        minlength: 1,
        trim:true 
    },
    option2: {
        type: String,
        required: true,
        minlength: 1,
        trim:true 
    },
    option3: {
        type: String,
        required: true,
        minlength: 1,
        trim:true 
    },
    option4: {
        type: String,
        required: true,
        minlength: 1,
        trim:true 
    },
    correctAnswer: {
        type: String,
        required: true,
        minlength: 1,
        trim:true 
    },
    published: {
        type: Boolean,
        default: false
    },
    createdAt: {
        type:String,
        default: Date
    }
})

// We create the Model and export it
const Question = mongoose.model('Question', QuestionSchema);

module.exports = { Question };
