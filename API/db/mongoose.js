// This file will handle the connecion logic to the MongoDB database

const mongoose = require('mongoose');

// We set mongoose to use global javascript Promise
mongoose.Promise = global.Promise;

//Flag for using new URL string parser instead of current (deprecated) one
mongoose.connect('mongodb://localhost:27017/QuizzApp', { useNewUrlParser: true }).then(() => {
    console.log("Connected to MongoDB succesully on port 27017");
}).catch((e) => {
    console.log("Error while attempting to connect to MongoDB");
    console.log(e);
});

// To prevent deprecation warning (from MongoDB native driver)
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

// Export the mongoose object
module.exports = { mongoose };

// MongoDB not connecting issue on macOS Catalina, solved link:
// https://github.com/mongodb/homebrew-brew/issues/22
// Following command on terminal: mongod --dbpath /usr/local/var/