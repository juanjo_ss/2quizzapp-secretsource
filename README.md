# Quizz App

This applicaction is a Quizz app using MEAN Stack development.

The application consists of a Quizz app where you can have register and login to play, you can see all the current published and unpublished questions, you only see a preview of the title, to answer the question you have to click on it and will be redirected to the question quizz page.

Questions can have to status published and unpublished, only the **ADMIN** have the rights to change the status of the questions, **delete, edit** them or **add new questions**. Also can manage all the **users/players** registered on the Quizz App application.

## Back End side

It consists of the following folders:
* **API**: Initilizates the server side providing all the CRUD operations methods and paths. Connect to the database and manage the data using express framework.
    * **db**: Models and db login connections.
    * **app.js**: Contains all the express logic to run the server side control with the CRUD operations HTTP methods (GET,POST,PATCH,DELETE).

## Front End side

It consists of the following folders:
* **Quizzfrontend/src/app**: Cliente side where is programmed the logic and specific usage of the application.
    * **app-routing-module.ts**: Define the components and routes paths.
    * **app.module.ts**: Declares and imports all the NgModules used.
    * The code is programmed to outsource the logic from the **web requests service** to the **web service** and then to the **web components**
        * **task.services.ts**: This task.service is responsible for modify all the data of the tasks
        * **web-request.service.ts**: This web-request.service is responsible of wrap all the requests HTTP methods and provide the URL as a constant to use it on the requests.
        * **/webpages**: Folder where is all the app functionallity programmed, login, admin, questions...
        * **/models**: Tasks and Lists models collection.


