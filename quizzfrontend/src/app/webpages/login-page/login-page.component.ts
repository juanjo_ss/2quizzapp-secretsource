import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/task.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  userArray: User[];

  user:User;

  constructor(private taskService: TaskService, private router: Router) { }


  ngOnInit() {

  }


  /**
   * Read the list of Users from the GET Method and then compare with the data from the form
   * check the specific Role of the user and then redirects 
   * @param username 
   * @param password 
   */
  logInUser(username: string, password: string) {

    // Use generics to cast it to the type we're expecting.
    // Notice the Generic of User casting the Type for resulting "data"
    this.taskService.getUsers().subscribe((data: User[]) => {
      this.userArray = data;
      for (var i = 0; i < this.userArray.length;) {
        this.user = this.userArray[i];
        console.log(this.user);
        if (this.user.username == username && this.user.password == password) { // If username and password is correct then route to an specific page
          if (this.user.roles[0] == "admin") {
            this.router.navigate(['/admin-page']);
            break;
          } else {
            this.router.navigate(['/home']); // Got to the HomePage
            break;
          }
        } else {
          i++;
          console.log("This User Do not Exist");
        }
      }
    });

  }
}
