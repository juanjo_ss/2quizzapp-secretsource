import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/task.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-users-list-page',
  templateUrl: './users-list-page.component.html',
  styleUrls: ['./users-list-page.component.scss']
})
export class UsersListPageComponent implements OnInit {

  constructor(private taskService: TaskService, private route: ActivatedRoute, private router: Router) { }

  users: User[];

  selectedUserID: string;

  ngOnInit() {

    // Get the route changes and will be returning the route parameters. Are really useful to get the value of the listID in the route.
    // As we .suscribe() to the observable whenever we click on the router link it will then 
    // notify the observable and get and update value of the listID
    this.route.params.subscribe(
      (params: Params) => {
        this.selectedUserID = params.questionID;
      }
    )
    /**
     * Get the all the Questions from GET Method, we save them into an Array that we read and
     * check for the value of published of each question
     */
    this.taskService.getUsers().subscribe((users: User[]) => {
      this.users = users;
    })
  }


  /**
   * Deletes the questions when we click on the trash icon
   * @param id we get the ._id of the object and delete the question
   */
  onUserDeleteClick(id:string) {
    this.taskService.deleteUser(id).subscribe((res:any) => {
      this.users = this.users.filter(val => val._id !== id);
      console.log(res);
      this.router.navigate(['/users-list']); // Redirected to the main page (if not we will have to refresh the page to see the delete)
    });
  }

}
