import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishedQuestionsQuizzComponent } from './published-questions-quizz.component';

describe('PublishedQuestionsQuizzComponent', () => {
  let component: PublishedQuestionsQuizzComponent;
  let fixture: ComponentFixture<PublishedQuestionsQuizzComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublishedQuestionsQuizzComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishedQuestionsQuizzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
