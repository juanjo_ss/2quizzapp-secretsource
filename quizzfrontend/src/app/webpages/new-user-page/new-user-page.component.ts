import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/task.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-new-user-page',
  templateUrl: './new-user-page.component.html',
  styleUrls: ['./new-user-page.component.scss']
})
export class NewUserPageComponent implements OnInit {

  constructor(private taskService: TaskService, private router: Router) { }

  ngOnInit() {
  }

  createUser(username: string, password: string, firstname: string, lastname: string, phone: string) {  
   

    this.taskService.createUser(username, password, firstname, lastname, phone).subscribe((userDoc) => {
 
      console.log(userDoc);
      // Now we navigate to /login page to check if we registered okey
      this.router.navigate(['/login']);
    })
  }
}
