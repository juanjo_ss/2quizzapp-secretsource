import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnpublishedQuestionsViewComponent } from './unpublished-questions-view.component';

describe('UnpublishedQuestionsViewComponent', () => {
  let component: UnpublishedQuestionsViewComponent;
  let fixture: ComponentFixture<UnpublishedQuestionsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnpublishedQuestionsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnpublishedQuestionsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
