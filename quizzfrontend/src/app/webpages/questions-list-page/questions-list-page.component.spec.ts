import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionsListPageComponent } from './questions-list-page.component';

describe('QuestionsListPageComponent', () => {
  let component: QuestionsListPageComponent;
  let fixture: ComponentFixture<QuestionsListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionsListPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionsListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
