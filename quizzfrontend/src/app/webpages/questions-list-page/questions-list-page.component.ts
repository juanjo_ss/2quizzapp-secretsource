import { Component, OnInit } from '@angular/core';
import { TaskService } from 'src/app/task.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Question } from 'src/app/models/question.model';

@Component({
  selector: 'app-questions-list-page',
  templateUrl: './questions-list-page.component.html',
  styleUrls: ['./questions-list-page.component.scss']
})
export class QuestionsListPageComponent implements OnInit {

  constructor(private taskService: TaskService, private route: ActivatedRoute, private router: Router) { }

  questions: Question[];

  selectedQuestionID: string;

  published: boolean = true;

  ngOnInit() {

    // Get the route changes and will be returning the route parameters. Are really useful to get the value of the listID in the route.
    // As we .suscribe() to the observable whenever we click on the router link it will then 
    // notify the observable and get and update value of the listID
    this.route.params.subscribe(
      (params: Params) => {
        this.selectedQuestionID = params.questionID;
      }
    )
    /**
     * Get the all the Questions from GET Method, we save them into an Array that we read and
     * check for the value of published of each question
     */
    this.taskService.getQuestions().subscribe((questions: Question[]) => {
      this.questions = questions;
    })
  }

  /**
   * Deletes the questions when we click on the trash icon
   * @param id we get the ._id of the object and delete the question
   */
  onQuestionDeleteClick(id: string) {
    this.taskService.deleteQuestion(id).subscribe((res: any) => {
      this.questions = this.questions.filter(val => val._id !== id);
      console.log(res);
      this.router.navigate(['/questions-list']); // Redirected to the main page (if not we will have to refresh the page to see the delete)
    });
  }

  /**
   * 
   * @param question 
   */
  onQuestionPublishedClick(question: Question) {

    if (question.published === true) {
      this.published = false;
    } else {
      this.published = true;
    }

    this.taskService.publishedQuestionState(question._id, this.published).subscribe((res) => {
      console.log(question);
      // We toggle the completed stage by clicking on it will have the opposite value of the current completed value to present on the webpage
      question.published = !question.published;
    });
  }

}
