import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { TaskService } from 'src/app/task.service';
import { Question } from 'src/app/models/question.model';

@Component({
  selector: 'app-published-questions-view',
  templateUrl: './published-questions-view.component.html',
  styleUrls: ['./published-questions-view.component.scss']
})
export class PublishedQuestionsViewComponent implements OnInit {

  constructor(private taskService: TaskService, private route: ActivatedRoute, private router: Router) { }

  questions: Question[];
  questionsPublished: Question[] = [];
  question: Question;

  ngOnInit() {

    /**
     * Get the all the Questions from GET Method, we save them into an Array that we read and
     * check for the value of published of each question
     */
    this.taskService.getQuestions().subscribe((questions: Question[]) => {
      this.questions = questions;
      var j = 0; // We use an local variable to fillup the questionPublished[] everytime we encounter an question.published===True
      for (var i = 0; i < this.questions.length; i++) {
        this.question = this.questions[i];
        if (this.question.published === true) { // If questions are publsihed they will show up on the list
          this.questionsPublished[j] = questions[i];
          j++;
        }
      }
      console.log(this.questionsPublished)
    })
  }


}
