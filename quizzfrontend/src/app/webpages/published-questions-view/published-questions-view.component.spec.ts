import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishedQuestionsViewComponent } from './published-questions-view.component';

describe('PublishedQuestionsViewComponent', () => {
  let component: PublishedQuestionsViewComponent;
  let fixture: ComponentFixture<PublishedQuestionsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublishedQuestionsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishedQuestionsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
