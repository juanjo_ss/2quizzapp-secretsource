import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewQuestionsPageComponent } from './new-questions-page.component';

describe('NewQuestionsPageComponent', () => {
  let component: NewQuestionsPageComponent;
  let fixture: ComponentFixture<NewQuestionsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewQuestionsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewQuestionsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
