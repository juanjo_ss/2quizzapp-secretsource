import { Injectable } from '@angular/core';
import { WebRequestService } from './web-request.service';
import { Question } from './models/question.model';
import { User } from './models/user.model';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private webRequestService: WebRequestService) { }


  /**
   * We get the list of Users
   */
  getQuestions() {
    return this.webRequestService.get('questions');
  }

  /**
   * We get the list of Users
   */
  getQuestionsByID(id:string) {
    return this.webRequestService.get(`questions/${id}`);
  }

  /**
   * We want to create a new question.
   * @param title 
   * @param option1 
   * @param option2 
   * @param option3 
   * @param option4 
   * @param correctAnswer 
   */
  createQuestion(title: string, question: Question) {
    return this.webRequestService.post('questions', question);
  }


  /**
   * Delete the question accesing it's ID
   */
  deleteQuestion(id: string) {
    return this.webRequestService.delete(`questions/${id}`);
  }

/**
 * Published the question accesing it's ID
 */
/*   publishedQuestion(question:Question, published:boolean) {
    return this.webRequestService.patch(`questions/${question._id}`, published);
  } */

  /**
 * Published the question accesing it's ID
 */
publishedQuestionState(id:string, published:boolean) {
  return this.webRequestService.patchQuestionState(`questions/${id}`, published);
}

  /**
   * We get the list of Users
   */
  getUsers() {
    return this.webRequestService.get('users');
  }

  user:User;
  /**
   * We want to create a new user.
   * @param username 
   * @param password 
   */
/*   createUser(username: string, password: string, firstname: string, lastname: string, phone: string) {
    var user:User = {
      _id:"", 
      username: username, 
      password: password, 
      firstname:firstname, 
      lastname:lastname, 
      phone:phone, 
      roles:[""]
    };
    console.log(user);
    return this.webRequestService.post('users', user);
  } */
  /**
   * We want to create a new user.
   * @param username 
   * @param password 
   */
  createUser(username: string, password: string, firstname: string, lastname: string, phone: string) {
    
/*     user.firstname = firstname;
    user.lastname = lastname;
    user.phone = phone; */ 
    return this.webRequestService.postUser('users', username, password, firstname, lastname, phone);
  }

  deleteUser(id:string) {
    return this.webRequestService.delete(`users/${id}`);
  }
}
