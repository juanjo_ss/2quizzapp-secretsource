import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Question } from './models/question.model';

@Injectable({
  providedIn: 'root'
})
export class WebRequestService {

  readonly ROOT_URL;

  constructor(private http: HttpClient) {
    this.ROOT_URL = 'http://localhost:3000';
  }

  get(uri: string) {
    return this.http.get(`${this.ROOT_URL}/${uri}`);
  }

  post(uri: string, payload: Object) {
    return this.http.post(`${this.ROOT_URL}/${uri}`, { payload });
  }

  patch(uri: string, payload: Object) {
    return this.http.patch(`${this.ROOT_URL}/${uri}`, { payload });
  }

  delete(uri: string) {
    return this.http.delete(`${this.ROOT_URL}/${uri}`);
  }

  
/* NOT THE HTTPS METHODS HOW I WANT BUT IT'S WORKING WITH THESE */

  patchQuestionState(uri: string, published: boolean) {
    return this.http.patch(`${this.ROOT_URL}/${uri}`, { published }, {
      observe: 'response'
    });
  }

  postUser(uri: string, username: string, password: string, firstname: string, lastname: string, phone: string) {
    return this.http.post(`${this.ROOT_URL}/${uri}`, {
      username,
      password,
      firstname,
      lastname,
      phone
    }, {
      observe: 'response'
    });
  }

  postQuestion(uri: string, title: string, option1: string, option2: string,
    option3: string, option4: string, correctAnswer: string) {
    return this.http.post(`${this.ROOT_URL}/${uri}`, {
      title,
      option1,
      option2,
      option3,
      option4,
      correctAnswer
    }, {
      observe: 'response'
    });
  }

  patchQuestion(uri: string, title: string, option1: string, option2: string,
    option3: string, option4: string, correctAnswer: string) {
      return this.http.patch(`${this.ROOT_URL}/${uri}`, {
      title,
      option1,
      option2,
      option3,
      option4,
      correctAnswer
    }, {
      observe: 'response'
    });
  }
}
