import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublishedQuestionsViewComponent } from './webpages/published-questions-view/published-questions-view.component';
import { UnpublishedQuestionsViewComponent } from './webpages/unpublished-questions-view/unpublished-questions-view.component';
import { LoginPageComponent } from './webpages/login-page/login-page.component';
import { AdminPageComponent } from './webpages/admin-page/admin-page.component';
import { HomePageComponent } from './webpages/home-page/home-page.component';
import { NewUserPageComponent } from './webpages/new-user-page/new-user-page.component';
import { UsersListPageComponent } from './webpages/users-list-page/users-list-page.component';
import { QuestionsListPageComponent } from './webpages/questions-list-page/questions-list-page.component';
import { PublishedQuestionsQuizzComponent } from './webpages/published-questions-quizz/published-questions-quizz.component';
import { NewQuestionsPageComponent } from './webpages/new-questions-page/new-questions-page.component';

const routes: Routes = [
  {path: '', redirectTo:'login', pathMatch:'full'},
  {path: 'home', component:HomePageComponent},
  {path: 'published-questions', component:PublishedQuestionsViewComponent},
  {path: 'unpublished-questions', component:UnpublishedQuestionsViewComponent},

  {path: 'published-questions-quizz/:questionID', component:PublishedQuestionsQuizzComponent},

  {path: 'login', component:LoginPageComponent},
  {path: 'admin-page', component:AdminPageComponent},

  {path: 'new-user', component:NewUserPageComponent},
  {path: 'new-question', component:NewQuestionsPageComponent},

  {path: 'users-list', component:UsersListPageComponent},
  {path: 'questions-list', component:QuestionsListPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
