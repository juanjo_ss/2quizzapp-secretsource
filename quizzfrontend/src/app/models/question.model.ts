export class Question {
    _id:string;
    title:string;
    option1: string;
    option2: string;
    option3: string;
    option4: string;
    correctAnswer: string;
    published:boolean;
    createdAt:string;
}
