export class User {
    _id:string;
    username:string;
    password:string;
    roles:string[];
    firstname:string;
    lastname:string;
    phone:string;
}