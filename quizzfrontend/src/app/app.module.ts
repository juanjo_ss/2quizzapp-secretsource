import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PublishedQuestionsViewComponent } from './webpages/published-questions-view/published-questions-view.component';
import { UnpublishedQuestionsViewComponent } from './webpages/unpublished-questions-view/unpublished-questions-view.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginPageComponent } from './webpages/login-page/login-page.component';
import { AdminPageComponent } from './webpages/admin-page/admin-page.component';
import { HomePageComponent } from './webpages/home-page/home-page.component';
import { NewUserPageComponent } from './webpages/new-user-page/new-user-page.component';
import { PublishedQuestionsQuizzComponent } from './webpages/published-questions-quizz/published-questions-quizz.component';
import { QuestionsListPageComponent } from './webpages/questions-list-page/questions-list-page.component';
import { UsersListPageComponent } from './webpages/users-list-page/users-list-page.component';
import { NewQuestionsPageComponent } from './webpages/new-questions-page/new-questions-page.component';

@NgModule({
  declarations: [
    AppComponent,
    PublishedQuestionsViewComponent,
    UnpublishedQuestionsViewComponent,
    LoginPageComponent,
    AdminPageComponent,
    HomePageComponent,
    NewUserPageComponent,
    PublishedQuestionsQuizzComponent,
    QuestionsListPageComponent,
    UsersListPageComponent,
    NewQuestionsPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule // Import the HTTP client module
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
